@extends("layouts/layouts")
@section("layouts")
    <div class="brand_car">
        <select class="brand_search">
            <option>Brand</option>
            @foreach($brand as $brand)
                <option value="{{$brand->id}}">{{$brand->brand}}</option>
            @endforeach
        </select>
    </div>
    <div class="area"></div>
    <div class="all">
        @foreach($cars as $car)
            <div class="car">
                <a href="{{URL::to('Car/'.$car->id)}}" class="href_show">
                    @if(count($car->image)>0)<span class="img_profile_car"><img src="{{URL::to('/storage/'.$car->image[0]->image)}}" width="30px" height="30px"></span>

                    @else
                        <span class="img_profile_car"><img src="{{URL::to('/img/avatar_car.png')}}" width="30px" height="30px"></span>
                    @endif
                        <span>Марка `<span class="car_brand_name">{{$car->model->brand->brand}}</span>  </span>
                        <span>Модель `<span class="model_car_name">{{$car->model->model}}</span></span>
                        <span>Дата ` <span class="date_car_name">{{$car->date}}</span></span>
                        <span>Цена ` <span class="price_car_name">{{$car->price}}</span></span>
                </a>
            </div>
        @endforeach
        <div class="empty"></div>
    </div>
    <div class="area"></div>
@endsection