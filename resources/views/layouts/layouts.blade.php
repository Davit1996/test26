<!Doctype html>
<html>
<head>
    <title>Registration</title>
    <link rel="stylesheet" href="{{URL::to('css/style.css')}}">
</head>
<body>
<header>
    @if(empty(Auth::id()))
        <span><a href="{{URL::to('/login')}}">Авторизоваться</a></span>
        <span><a href="{{URL::to('/registration')}}">Регистрация</a></span>
    @else
        @if(Auth::user()->type === "Admin")
            <span><a href="{{URL::to('User')}}">Пользователей</a></span>
            <span><a href="{{URL::to('Brand')}}">Марка</a></span>
        @else
            <span><a href="{{URL::to('Car/create')}}">Создайте</a></span>
            <span><a href="{{URL::to('User/edit')}}">Редактировать машину</a></span>
        @endif
            <span><a href="{{URL::to('/user/dashboard')}}">Дома</a></span>
        <span><a href="{{URL::to('user/logout')}}">Выйти</a></span>
    @endif
        <span><a href="{{URL::to('car/index')}}">Автомобили</a></span>
</header>
<input type="hidden" id="token" value="{{csrf_token()}}">

<div class="area"></div>
@yield("layouts")
<div class="area"></div>
</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="{{URL::to('/js/script.js')}}"></script>
</html>