@extends("layouts/layouts")
@section("layouts")
    @foreach($users as $user)
        <div class="users">
            <div class="delete_user" data-id="{{$user->id}}">Delete</div>
            <div>
                <img src="{{URL::to('/img/avatar_user.png')}}">
            </div>
            <div>
                <h1>{{$user->name." ".$user->surname}}</h1>
                <h1 class="age_user">{{$user->age}}</h1>
            </div>
        </div>
        <br>
    @endforeach
@endsection