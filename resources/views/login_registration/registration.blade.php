@extends("layouts/layouts")
@section("layouts")
    <form class="form"  method="post" action="{{URL::to('login_registration')}}">
        <h1>Регистрация</h1>
        <div>
            @if($errors->has("name"))
                <span>{{$errors->first("name")}}</span>
            @endif
        </div>
        <input type="text" name="name" placeholder="название">
        <div>
            @if($errors->has("surname"))
                <span>{{$errors->first("surname")}}</span>
            @endif
        </div>
        <input type="text" name="surname" placeholder="фамилия">
        <div>
            @if($errors->has("age"))
                <span>{{$errors->first("age")}}</span>
            @endif
        </div>
        <input type="number" name="age" placeholder="возраст">
        <div>
            @if($errors->has("email"))
                <span>{{$errors->first("email")}}</span>
            @endif
        </div>
        <input type="email" name="email" placeholder="электронное письмо">
        <div>
            @if($errors->has("password"))
                <span>{{$errors->first("password")}}</span>
            @endif
        </div>
        <input type="password" name="password" placeholder="пароль">
        <div>
            @if($errors->has("cnfpassword"))
                <span>{{$errors->first("cnfpassword")}}</span>
            @endif
        </div>
        <input type="password" name="cnfpassword" placeholder="Подтвердите пароль">
        <div></div>
        @csrf
        <button>Отправить</button>
    </form>
@endsection