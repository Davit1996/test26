<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ModelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("model")->insert([
            [
                "model" => "525i",
                "brand_id" => "1",
            ],[
                "model" => "325i",
                "brand_id" => "1",
            ],[
                "model" => "x6",
                "brand_id" => "1",
            ],[
                "model" => "March",
                "brand_id" => "2",
            ],[
                "model" => "x-trail",
                "brand_id" => "2",
            ],[
                "model" => "Terrano",
                "brand_id" => "2",
            ],[
                "model" => "C-180",
                "brand_id" => "3",
            ],[
                "model" => "G-55",
                "brand_id" => "3",
            ],[
                "model" => "CL-350",
                "brand_id" => "3",
            ],[
                "model" => "A7",
                "brand_id" => "4",
            ],[
                "model" => "A3",
                "brand_id" => "4",
            ],[
                "model" => "Q3",
                "brand_id" => "4",
            ]
            ]);
    }
}
