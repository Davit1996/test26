<?php

use Illuminate\Database\Seeder;
use App\Users;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Users::Insert([
            "name" => "Armen",
            "surname" => "Makaryan",
            "age" => "23",
            "type" => "Admin",
            "email" => "Armen@mail.ru",
            "password" => Hash::make("0123456789"),
        ]);
        Users::Insert([
            "name" => "Tigran",
            "surname" => "Hakopyan",
            "age" => "23",
            "email" => "Tigran@mail.ru",
            "password" => Hash::make("987654321"),
        ]);
        Users::Insert([
            "name" => "Henrik",
            "surname" => "Markosyan",
            "age" => "23",
            "email" => "Henrik@mail.ru",
            "password" => Hash::make("00000000"),
        ]);

    }
}
