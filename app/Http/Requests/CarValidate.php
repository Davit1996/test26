<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CarValidate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "color" => "required",
            "date" => "required|numeric|max:100000000",
            "mileage" => "required|numeric|max:100000000",
            "price" => "required|numeric|max:100000000",
        ];
    }
}
