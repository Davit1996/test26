<?php

namespace App\Http\Controllers;

use App\Users;
use App\Car;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
use View;

class UserController extends Controller
{
    public function index(){
        $users = Users::where(["type"=>"user"])->get();
        return view("user/index",compact("users"));
    }

    public function show(){
        $cars = Car::where(["users_id"=>auth()->id()])->get();
        return view("car/edit_car",compact("cars"));
    }

    public function destroy($id){
        Users::where("id",$id)->delete();
    }

    public function dashboard(){
        return view("user/dashboard");
    }

    public function logout(){
        Auth::logout();
        return Redirect::to('/login');
    }
}
