<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    protected $table = "brand";
    public $timestamps = false;
    protected $fillable = [
        "brand",
    ];

    public function model(){
            return $this->hasMany("App\Models","brand_id");
    }

}
