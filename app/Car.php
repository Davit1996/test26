<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    protected $table = "car";
    public $timestamps = false;
    protected $fillable = [
        "color",
        "date",
        "mileage",
        "price",
        "model_id",
        "users_id",
        "brand_id",
    ];

    public function model(){
        return $this->belongsTo("App\Models","model_id");
    }
    public function image(){
        return $this->hasMany("App\Image","car_id");
    }
}
